export const GET_TODOS = 'todo/getTodoList/LOAD';
export const GET_TODOS_SUCCESS = 'todo/getTodoList/LOAD_SUCCESS';
export const GET_TODOS_ERROR = 'todo/getTodoList/LOAD_ERROR';

export const EDIT_TODO = 'todo/editTodoList/LOAD';
export const EDIT_TODO_SUCCESS = 'todo/editTodoList/LOAD_SUCCESS';
export const EDIT_TODO_ERROR = 'todo/editTodoList/LOAD_ERROR';

export const CREATE_TODO = 'todo/createTodoList/LOAD';
export const CREATE_TODO_SUCCESS = 'todo/createTodoList/LOAD_SUCCESS';
export const CREATE_TODO_ERROR = 'todo/createTodoList/LOAD_ERROR';

export const REMOVE_TODO = 'todo/removeTodoList/LOAD';
export const REMOVE_TODO_SUCCESS = 'todo/removeTodoList/LOAD_SUCCESS';
export const REMOVE_TODO_ERROR = 'todo/removeTodoList/LOAD_ERROR';
