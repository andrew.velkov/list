import React from 'react';
import { Grid, IconButton, Icon } from '@material-ui/core';

import css from 'styles/components/ListItem.scss';

const ListItem = ({ todo, onClick }) => (
  <Grid container justify="flex-start" alignItems="center" className={css.nth}>
    <Grid item xs={4} sm={4}>
      <p className={css.nowrap}>{todo.title}</p>
    </Grid>
    <Grid item xs={5} sm={6}>
      <p className={css.nowrap}>{todo.body}</p>
    </Grid>
    <Grid item xs={2} sm={1}>
      <p>{todo.date}</p>
    </Grid>
    <Grid container justify="flex-end" item xs={1} sm={1}>
      <IconButton onClick={onClick} aria-label="Delete">
        <Icon fontSize="small">delete</Icon>
      </IconButton>
    </Grid>
  </Grid>
);

export default ListItem;
