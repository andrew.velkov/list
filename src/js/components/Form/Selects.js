import React from 'react';
import { TextField, MenuItem } from '@material-ui/core';

const Selects = ({
  data, label, name, value, onChange,
}) => (
  <TextField
    label={label}
    name={name}
    select
    fullWidth
    value={value}
    onChange={onChange}
  >
    <MenuItem key="none" value="none">None</MenuItem>
    {data.map(item => (
      <MenuItem key={item.id} value={item.value}>
        {item.name}
      </MenuItem>
    ))}
  </TextField>
);

export default Selects;
