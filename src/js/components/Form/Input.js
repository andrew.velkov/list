import React from 'react';
import { TextField } from '@material-ui/core';

const Input = ({
  label, name, value, onChange, error, placeholder, ...input
}) => (
  <TextField
    label={label}
    name={name}
    value={value}
    // variant="filled"
    fullWidth
    margin="normal"
    placeholder={placeholder}
    onChange={onChange}
    {...input}
  />
);

export default Input;
