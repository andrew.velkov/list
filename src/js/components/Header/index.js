import React from 'react';

import css from 'styles/components/Header.scss';

const Header = () => (
  <header className={css.header}>
    <section className={css.wrapper}>
      <h2>To Do List</h2>
    </section>
  </header>
);

export default Header;
