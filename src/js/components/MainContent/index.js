import React from 'react';
import cx from 'classnames';
import css from 'styles/components/MainContent.scss';

const MainContent = ({ children }) => (
  <main className={css.main}>
    <section className={cx(css.wrapper, css.main__wrap)}>
      {children}
    </section>
  </main>
);

export default MainContent;
