import React from 'react';
import cx from 'classnames';
import {
  Dialog, DialogActions, DialogContent, Fab, DialogTitle, Icon,
} from '@material-ui/core';

import Button from 'components/Buttons';

import css from 'styles/components/Dialog.scss';

export default class Modals extends React.Component {
  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({
      open: true,
    });
  };

  handleClose = () => {
    this.setState({
      open: false,
    });
  };

  render() {
    const {
      title, onClick, disabled, children,
    } = this.props;
    const { open } = this.state;

    return (
      <React.Fragment>
        <Fab
          className={cx(css.dialog__button, css.dialog__button_fix)}
          color="primary"
          aria-label="Add"
          onClick={this.handleClickOpen}
        >
          <Icon>add</Icon>
        </Fab>

        <Dialog
          open={open}
          fullWidth
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">
            {title}
          </DialogTitle>
          <DialogContent>
            {children}
          </DialogContent>
          <DialogActions>
            <Button onClick={onClick} color="secondary" disabled={disabled} variant="contained">Send</Button>
            <Button onClick={this.handleClose} color="default" variant="text">Cancel</Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}
