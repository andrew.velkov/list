import React from 'react';

import css from 'styles/components/Footer.scss';

const Footer = () => (
  <footer className={css.footer}>
    <section className={css.wrapper}>
      <small>@ 2019</small>
    </section>
  </footer>
);

export default Footer;
