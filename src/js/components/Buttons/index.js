import React from 'react';
import Button from '@material-ui/core/Button';

const Buttons = ({
  variant = 'contained', color = 'primary', size = 'medium', children, onClick, ...button
}) => (
  <React.Fragment>
    <Button variant={variant} color={color} size={size} onClick={onClick} {...button}>
      {children}
    </Button>

    {variant === ''
      && (
        <Button color={color} size={size} onClick={onClick} {...button}>
          {children}
        </Button>
      )}
  </React.Fragment>
);

export default Buttons;
