import React from 'react';
import { Grid, withStyles } from '@material-ui/core';

import Header from 'components/Header';
import Footer from 'components/Footer';
import MainContent from 'components/MainContent';

const styles = () => ({
  mainContent: {
    height: '100%',
    flexWrap: 'nowrap',
  },
});

const App = ({ classes, ...props }) => (
  <Grid container direction="column" justify="space-between" alignItems="stretch" className={classes.mainContent}>
    <Header />
    <MainContent {...props} />
    <Footer />
  </Grid>
);

export default withStyles(styles)(App);
