const categories = [
  {
    id: 1,
    name: 'Phone Calls',
    value: 'phone_calls',
    icon: 'perm_phone_msg',
  },
  {
    id: 2,
    name: 'Errands',
    value: 'errands',
    icon: 'accessibility_new',
  },
  {
    id: 3,
    name: 'Correspondents',
    value: 'correspondence',
    icon: 'mail_outline',
  },
  {
    id: 4,
    name: 'Projects',
    value: 'projects',
    icon: 'settings_applications',
  },
  {
    id: 5,
    name: 'Miscellaneous Tasks',
    value: 'miscellaneous_tasks',
    icon: 'insert_drive_file',
  },
];

export default categories;
