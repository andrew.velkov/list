import * as type from 'constants/todo';

export const getTodoList = () => (
  {
    types: [type.GET_TODOS, type.GET_TODOS_SUCCESS, type.GET_TODOS_ERROR],
    payload: {
      request: {
        url: '/todo',
        method: 'GET',
      },
    },
  }
);

// export const editTodoList = (id, data) => (
//   {
//     types: [type.EDIT_TODO, type.EDIT_TODO_SUCCESS, type.EDIT_TODO_ERROR],
//     payload: {
//       request: {
//         url: `/todo/${id}`,
//         method: 'PUT',
//         headers: {
//           'Content-Type': 'application/json',
//         },
//         data,
//       },
//     },
//   }
// );

export const createTodoList = data => (
  {
    types: [type.CREATE_TODO, type.CREATE_TODO_SUCCESS, type.CREATE_TODO_ERROR],
    payload: {
      request: {
        url: '/todo',
        method: 'POST',
        data,
      },
    },
  }
);

export const removeTodoList = id => (
  {
    types: [type.REMOVE_TODO, type.REMOVE_TODO_SUCCESS, type.REMOVE_TODO_ERROR],
    payload: {
      request: {
        url: `/todo/${id}`,
        method: 'DELETE',
      },
    },
  }
);
