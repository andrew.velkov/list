import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Icon } from '@material-ui/core';

import { getTodoList, removeTodoList, createTodoList } from 'actions';
import Fetching from 'components/Fetching';
import Dialog from 'components/Dialog';
import Input from 'components/Form/Input';
import Select from 'components/Form/Selects';
import ListItem from 'components/ListItem';
import categories from 'config/categories';

import css from 'styles/pages/Main.scss';

class MainPage extends Component {
  state = {
    title: '',
    body: '',
    date: '',
    categorySelected: 'none',
  };

  componentDidMount() {
    const { getTodoListData } = this.props;
    getTodoListData();
  }

  handleChange = (e) => {
    const { name, value } = e.target;

    this.setState({
      [name]: value,
    });
  }

  createTodoHandler = (e) => {
    e.preventDefault();
    const { createTodoListData, getTodoListData } = this.props;
    const { categorySelected } = this.state;
    const data = { ...this.state, key: categorySelected };
    createTodoListData(data)
      .then((res) => {
        if (res.payload.status === 201) {
          getTodoListData();
          this.setState({
            title: '',
            body: '',
            date: '',
            categorySelected: 'none',
          });
        }
      });
  }

  removeItemHandler = (e, todoId) => {
    e.preventDefault();
    const { removeTodoListId, getTodoListData } = this.props;
    removeTodoListId(todoId)
      .then(res => res.payload.status === 200 && getTodoListData());
  }

  render() {
    const { getTodo: { data, loading, loaded } } = this.props;
    const {
      categorySelected, title, body, date,
    } = this.state;
    const isDisabled = categorySelected === 'none' || title === '';

    return (
      <React.Fragment>
        <Dialog
          title="Create Todo List"
          onClick={this.createTodoHandler}
          fixed
          disabled={isDisabled}
        >
          <Select
            label="Select category"
            name="categorySelected"
            data={categories}
            value={categorySelected}
            onChange={this.handleChange}
          />
          <Input
            label="Title"
            name="title"
            value={title}
            onChange={this.handleChange}
          />
          <Input
            label="Description"
            name="body"
            value={body}
            onChange={this.handleChange}
          />
          <Input
            label="Date"
            name="date"
            value={date}
            onChange={this.handleChange}
          />
        </Dialog>

        <Fetching isFetching={loading}>
          {(loaded && data.length > 0) && categories.map(category => (
            <article key={category.id} className={css.todo}>
              <div className={css.todo__icon}>
                <Icon>{category.icon}</Icon>
              </div>
              <div className={css.todo__category}>
                <h3>{category.name}</h3>
                <div className={css.todo__layout}>
                  <ul className={css.todo__table}>
                    {loaded && data.filter(item => item.key === category.value).map(todo => (
                      <ListItem
                        key={todo.id}
                        todo={todo}
                        onClick={(e) => { this.removeItemHandler(e, todo.id); }}
                      />
                    ))}
                  </ul>
                </div>
              </div>
            </article>
          ))}

          {data.length <= 0
            && (
            <div className={css.well}>
              <p>No results...</p>
            </div>
            )}
        </Fetching>
      </React.Fragment>
    );
  }
}

export default connect(state => ({
  getTodo: state.get.getTodoList,
}), dispatch => ({
  getTodoListData: () => dispatch(getTodoList()),
  removeTodoListId: id => dispatch(removeTodoList(id)),
  createTodoListData: data => dispatch(createTodoList(data)),
}))(MainPage);
