import { combineReducers } from 'redux';

import todo from './todo';

const reducers = combineReducers({
  get: combineReducers({
    getTodoList: todo('getTodoList'),
    removeTodoList: todo('removeTodoList'),
  }),
});

export default reducers;
