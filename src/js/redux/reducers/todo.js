const initialState = {
  loaded: false,
  loading: false,
  data: [],
};

export default function todo(params = '') {
  return (state = initialState, action = {}) => {
    switch (action.type) {
      case `todo/${params}/LOAD`:
        return {
          ...state,
          loading: true,
          loaded: false,
        };
      case `todo/${params}/LOAD_SUCCESS`:
        return {
          ...state,
          loading: false,
          loaded: true,
          data: action.payload.data,
        };
      case `todo/${params}/LOAD_ERROR`:
        return {
          ...state,
          loading: false,
          loaded: false,
        };
      default:
        return state;
    }
  };
}
