import React, { Suspense, lazy } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Loader from 'components/Loader';

const App = lazy(() => import('containers/App'));
const MainPage = lazy(() => import('pages/MainPage'));
const NotFoundPage = lazy(() => import('pages/NotFoundPage'));

const Routes = () => (
  <BrowserRouter>
    <Suspense fallback={<Loader isFetching />}>
      <App>
        <Switch>
          <Route exact path="/" render={props => <MainPage {...props} />} />
          <Route component={NotFoundPage} />
        </Switch>
      </App>
    </Suspense>
  </BrowserRouter>
);

export default Routes;
